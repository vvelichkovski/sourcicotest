'use strict';
const userController = require('../controllers/users.controller');

module.exports = (app) => {
    app.get('/users', userController.listUsers);
    app.post('/user', userController.createUser);

    // Long Polling
    const longpoll = require("express-longpoll")(app);
    longpoll.create("/userchanges");
}